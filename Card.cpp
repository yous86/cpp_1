#include <iostream>
#include "Card.h"


// Overload constructor
Card::Card(std::string name, int mana_cost, CardType type) {
    name_ = name;
    mana_cost_ = mana_cost;
    type_ = type;
}

// Methods
std::string Card::getName() { return name;}

CardType Card::getType() { return type;}

int Card::getManaCost() { return mana_cost;}


