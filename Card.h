#include <iostream>
#include <cstring>
#ifndef CARD_H
#define CARD_H

// TODO: check enum inside the class?
enum CardType {
    CREATURE,
    SPELL
};

class Card {
private:
    // Instance's variables
    std::string name_;
    int mana_cost_;
    CardType type_;


public:
    // Overload constructor
    Card(std::string, int, CardType);

    // Methods
    std::string getName();
    CardType getType();
    int getManaCost();

};


#endif //CARD_H
