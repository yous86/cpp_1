#ifndef PLAYER_H
#define PLAYER_H
// TODO: include vector?
#include <iostream>
class Player {
private:
    // Instance's variables
    std::string name_;
    int life_points_; //30 life points each player in the beginning
    int mana_points_; //0 Mana points each player in the beginning
    // TODO: add attributes game_field_ (Array 7 pointer) pick_up_stack_
    //  (Vector pointer), hand_ (Vector pointer), graveyard_ (Vector pointer)

public:
    // Overload constructor
    Player(std::string name){
        name_ = name;
        life_points_ = 30;
        mana_points_ = 0;
        //TODO:
        // other steps for game_field_, pick_up_stack_, hand_, graveyard_
    }

    // Methods
    // TODO:
    //  1. check date type for the function
    //  2. getHandCards() and GameField() return std::vector and const **
    std::string getName();
    int getLifePoints();
    void addLifePoints(int points);
    int getManaPoints();
    void addMana(int mana);
    void reduceMana(int mana);
    void getHandCards();
    int getHandSize();
    void getGameField();


};


#endif //PLAYER_H

