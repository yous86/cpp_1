# Assignment 1 - Deliverable 2

Die allgemeine Spielbeschreibung ist unter [README.md](../README.md) nachzulesen.



Im Deliverable 2 soll das Config-File eingelesen und verarbeitet werden. 

Das Config-File ist eine JSON-Datei, welche mit der vom Framework mitgegebenen [`RapidJSON`](https://rapidjson.org/) Bibliothek geparsed werden kann und somit ein leichtes Erstellen der Karten ermöglichen soll.

Die Bibliothek kann über `#include "rapidjson/istreamwrapper.h" ` verwenden werden. Nähere Informationen zur Verwendung von [`RapidJSON`](https://rapidjson.org/) ist unter [`rapidjson.org`](https://rapidjson.org/)  zu finden.

Ihr könnt natürlich auch euren eigenen JSON-Parser schreiben, um die Config-Files einzulesen.

Ein valides Config-Files liegt unter [`config/`](../config/config01.json).



## ConfigFile

### Bedingungen für ein gültiges Config-File

- Mindestens 10 Karten

- Bezeichnung der Karte und Name der Karte müssen immer gleich sein

  - `"Healer": { "name": "Zauber"}` ist z.B. invalide.
  - Alle Namen sind case-insensitive, das heißt Groß- und Kleinschreibung wird nicht beachtet.

- Das Config-File muss eingeteilt in 2 Teile geteilt sein: `Creatures` und `Spells` (siehe Beispiel unten).

  - In `Creatures` dürfen nur Kreaturenkarten vorkommen.
  - In `Spells` dürfen nur Zaubersprüche vorkommen.

- Kreaturkarten erfüllen folgende Bedingungen:

    - Schadenswert ist eine Ganzzahl im Bereich 0-9 und Lebenspunkte ist einen Ganzzahl im Bereich 1-9.
    - Kreaturen haben einen eindeutigen Namen mit maximal Länge 8 Zeichen.
    - Es müssen alle 6 Attribute vorhanden sein: 
        - name
        - mana_cost
        - damage_points
        - life_points
        - shield
        - mana_drain 

- Zaubersprüche erfüllen folgende Bedingungen:

    - Zaubersprüche haben nur einen eindeutigen Namen mit maximal Länge 8 Zeichen.

        - Es darf kein zusätzliches Attribut vorhanden sein.

    - Name muss exakt einer der folgenden 4  sein und dem enum SpellType zuordnerbar:

        - ```
          enum SpellType {HEALER, RELIEF, REBIRTH, DRACULA}
          ```

        - name: "Healer" => `SpellType.HEALER`

        - name: "Relief" => `SpellType.RELIEF`

        - name: "Rebirth" => `SpellType.REBIRTH`

        - name: "Dracula" => `SpellType.DRACULA`

        - wobei der name im ConfigFile gleich dem Variablennamen in enum ist (case-insensitive).

        - Stimmt der Name **nicht** mit einem Namen im Framework überein, so ist das ConfigFile invalide.

- Karten **dürfen** öfters vorkommen, jedoch:

      - Kreaturkarten mit **gleichen** Namen müssen **gleiche Attributwerte** (Lebenspunkte, Schaden...) haben.

Trifft eine dieser Bedignungen NICHT zu, so ist das Config-File ungültig und das Programm soll -3 zurückgeben (Oop::RETURN_ERROR_BAD_CONFIG).



### Aufbau

Es gibt 2 Arten von Karten - Kreaturen (Creatures) und Zaubersprüche (Spells)

Beispiel für ein Config-File:

```
{
  "Creatures" : {
    "Big Wall": {
      "name": "Big Wall",
      "mana_const": 7,
      "damage_points": 2,
      "life_points": 9,
      "shield": true,
      "mana_drain": false
    },
    "Werwolf": {
      "name": "Werwolf",
      "mana_const": 13,
      "damage_points": 9,
      "life_points": 6,
      "shield": false,
      "mana_drain": false
    },
    "Golem": {
      "name": "Golem",
      "mana_const": 8,
      "damage_points": 7,
      "life_points": 8,
      "shield": false,
      "mana_drain": false
    },
    "Fee": {
      "name": "Fee",
      "mana_const": 1,
      "damage_points": 1,
      "life_points": 1,
      "shield": false,
      "mana_drain": false
    },
    "Elf": {
      "name": "Elf",
      "mana_const": 4,
      "damage_points": 3,
      "life_points": 4,
      "shield": false,
      "mana_drain": false
    },
    "Elf": {
      "name": "Elf",
      "mana_const": 4,
      "damage_points": 3,
      "life_points": 4,
      "shield": false,
      "mana_drain": false
    },
    "Elf": {
      "name": "Elf",
      "mana_const": 4,
      "damage_points": 3,
      "life_points": 4,
      "shield": false,
      "mana_drain": false
    },
    "Elf": {
      "name": "Elf",
      "mana_const": 4,
      "damage_points": 3,
      "life_points": 4,
      "shield": false,
      "mana_drain": false
    },
    "Elf": {
      "name": "Elf",
      "mana_const": 4,
      "damage_points": 3,
      "life_points": 4,
      "shield": false,
      "mana_drain": false
    },
    "Elf": {
      "name": "Elf",
      "mana_const": 4,
      "damage_points": 3,
      "life_points": 4,
      "shield": false,
      "mana_drain": false
    }
  },
  "Spells" : {
    "Healer": {
      "name": "Healer"
    },
    "Relief": {
      "name": "Relief"
    },
    "Rebirth": {
      "name": "Rebirth"
    },
    "Dracula": {
      "name": "Dracula"
    }
  }
}
```

