# Assignment 1 - Deliverable 3

Im Deliverable 3 soll die gesamte Spiellogik implementiert werden. Ist das Config-File eingelesen, kann das Spiel starten. Um jedoch spielen zu können, benötigt man Commands, die über das CLI eingegeben werden können. Diese Commands sollen nun im D3 implementiert werden.

Die allgemeine Spielbeschreibung ist unter [README.md](../README.md) nachzulesen.





# Benutzereingabe

### Eingabeaufforderung

- Zu Beginn des Spiels soll für jeden Spieler der Name über `std::cin` eingegeben und dem Attribut name_ des entsprechenden Spielers zugewiesen werden.

- Der Standardprompt `oop1>` wird ausgegeben, bis Spielernamen festgelegt sind.

- Sobald das Spiel beginnt, soll jeweils der Name des jeweiligen Spielers ausgegeben werden, welcher gerade am Zug ist.
    - `christian>` wenn name_ des Spielers, der gerade an der Reihe ist, "christian" lautet.
    - `player2>` when name_ des Spielers, der gerade an der Reihe ist, "player2" lautet.

- Wird kein Befehl (nur Enter gedrückt) eigegeben, so soll die Schleife erneut beginnen (erneute Ausgabe des Standardprompts)

### Befehle

**Allgemein gilt:**

- Hat man für einen Befehl nicht genügend Mana, soll folgende Fehlermeldung ausgegeben werden:

  ```
  [INFO] Not enough mana for this move!\n
  ```

- Treffen mehrere Fehlermeldungen bei einem Command gleichzeitig zu, ist nur jene mit der höchsten Priorität auszugeben. Die Priorität entspricht der Reihenfolge in [Fehlermeldungen](ErrorDescription.md).

- Alle Befehle sind case-insensitive, das heißt Groß- und Kleinschreibung wird nicht beachtet.
- In eckige Klammern geschriebene Buchstaben [X] [Y] sind die Paramter dieser zu implementierenden Funktion.

#### help

```
help 
```

gibt den String unter Oop::Interface::INFO_HELP_MSGS aus.

#### attack [X] [Y]

Die Kreatur an der Position Y am eigenen Spielfeld greift die Kreatur an der Position X des Spielfeldes des Gegners an. Eine Kreatur kann nur 1 Mal pro Runde angreifen.

```
attack X with Y
```

###### Parameter

- **X**: Falls der Wert X in der Menge {1, 2, ..., 7} ist, so gibt X die Position der Kreatur am gegnerischen Spielfeld an, die angegriffen werden soll. Wenn X gleich 0 ist, soll der Gegner direkt angegriffen werden.
    - Mögliche Werte: 0 bis 7

- **Y**: Position auf dem (eigenen) Spielfeld jener Kreatur, die angreifen soll.
    - Mögliche Werte: 1 bis 7
    - 0 ist kein möglicher Wert, da 0 der Index des eigenen Spielers ist. Spieler können jedoch nicht selbst angreifen; nur Kreaturen können angreifen.

###### Fehler

Ist einer der Parameter außerhalb des vorgegebenen Wertebereichs soll folgende Fehlermeldung ausgegeben werden:

```
[INFO] Wrong command parameter!\n
```

Sind beide Parameter innerhalb des vorgegebenen Wertebereichs und kann der Befehl nicht ausgeführt werden, weil z.B.

- die Position am eigenen Spielfeld leer ist oder
- die Position auf dem gegnerischen Spielfeld leer ist oder
- das Monster in dieser Runde schon angriffen hat,

 soll folgende Fehlermeldung ausgegeben werden:

```
[INFO] execution not possible!\n
```

Wird versucht, den Gegner direkt oder eine Kreatur ohne Schild anzugreifen, obwohl der Gegner über eine Schild-Kreatur verfügt, so ist der Angriff nicht möglich und es soll folgende Fehlermeldung ausgegeben werden:

```
[INFO] destroy all shields first!\n
```

#### set [X] [Y]

Setzt die Kreatur an Position X auf der Hand des ziehenden Spielers an die Position Y seines Spielfelds. Mit dem set-Command können nur Kreaturen ausgespielt weden. Für Zauberkarten ist der cast-Command zu implementieren.

```
set X to Y
```



###### Parameter

- **X**: Position der Kreatur auf der Hand (Handkarten) des ziehenden Spielers
    - Mögliche Werte: 1 bis 7
    - 0 ist kein möglicher Wert, da die erste Handkarte die Position 1 hat.

- **Y**: freie Position am eigenen Spielfeld, auf die die Kreatur gelegt werden soll
    - Mögliche Werte: 1 bis 7 (die Position muss außerdem frei sein!)
    - 0 ist kein möglicher Wert, da sich an dieser Position bereits der eigene Spieler befindet.

Die Kreaturen können beliebig auf das Spielfeld gelegt werden, es ist also nicht notwendig, zuerst die Positionen von 1 bis (X-1) mit Kreaturen zu füllen.

###### Aufgaben

- wird eine Karte ausgespielt, müssen die dahinterliegenden Karten nachrücken, sodass bei  **X** Karten auf der Hand immer genau die Nummern 1 bis X für die Karten genutzt werden.
- Kreaturen werden beim Ausspielen [beschworen (README.md#Beschwörung)](../README.md), das heißt sie schlafen eine Runde. Das heißt sie können im Zug ihres Ausspielens (mit `set`) noch nicht für einen Angriff genutzt werden.

###### Fehler

Ist einer der Parameter außerhalb des vorgegebenen Wertebereichs soll folgende Fehlermeldung ausgegeben werden:

```
[INFO] Wrong command parameter!\n
```

Sind beide Parameter innerhalb des vorgegebenen Wertebereichs und kann der Befehl nicht ausgeführt werden, weil z.B.

- die Position am Spielfeld belegt ist oder
- die Position auf der Hand keine Kreaturenkarte ist,

 soll folgende Fehlermeldung ausgegeben werden:

```
[INFO] execution not possible!\n
```



#### cast [X]

Führt den Zauber an der Position X auf der Hand des ziehenden Spielers auf.

```
cast X
```

###### Parameter

- **X**: Position der Zauberkarte auf der Hand (Handkarten) des ziehenden Spielers
    - Mögliche Werte: 1 bis 7

###### Fehler

Ist der Parameter außerhalb des vorgegebenen Wertebereichs, soll folgende Fehlermeldung ausgegeben werden:

```
[INFO] Wrong command parameter!\n
```

Ist der Parameter innerhalb des vorgegebenen Wertebereichs und kann der Befehl nicht ausgeführt werden, weil z.B.

- die Position auf der Hand keine Zauberkarte ist

 soll folgende Fehlermeldung ausgegeben werden:

```
[INFO] execution not possible!\n
```



#### sacrifice [X]

Opfert die Karte an Position X auf der Hand des ziehenden Spielers UND erhöht seine Lebenspunkte +1. Man kann Kreaturen und Zauberkarten opfern. Kreaturenkarten werden auf den Friedhof gelegt; Zauberkarten verschwinden.

```
sacrifice X
```

###### Parameter

- **X**: Position der Karte auf der Hand (Handkarten) des ziehenden Spielers
    - Mögliche Werte: 1 bis 7

###### Fehler

Ist der Parameter außerhalb des vorgegebenen Wertebereichs soll folgende Fehlermeldung ausgegeben werden:

```
[INFO] Wrong command parameter!\n
```

Ist der Parameter innerhalb des vorgegebenen Wertebereichs und kann der Befehl nicht ausgeführt werden, weil z.B.

- die Position auf der Hand leer ist (keine Karte),

 soll folgende Fehlermeldung ausgegeben werden:

```
[INFO] execution not possible!\n
```



#### finish

```
finish
```

Beendet deinen eigenen Spielzug und signalisiert dem Gegner, wieder dran zu sein.



#### quit

```
quit
```

Beendet das gesamte Programm mit Rückgabewert 0.



### Log

Das Log-File soll beim Starten des Clients erstellt werden und in der Folge Zeile für Zeile **alle** vom Spieler eingegebenen Befehle mit `\n` getrennt in das File schreiben.