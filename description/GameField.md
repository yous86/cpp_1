# Spielfeld

Das Spielfeld wird vom Framework zur Verfügung gestellt, damit sich bei Studierenden keine unnötigen Leerzeichen-Fehler oder Ähnliches, was zu Punkteabzügen führt, einschleichen kann.

Das Spielfeld kann mittels Funktionsauruf **io_.out(Oop::Player, Oop::Player)** des Interfaces geprintet werden.





## Darstellung

### Karte

```
0Z____MS     0Z____MS
|      |     |      |
ZAUBERER     GNOM
|  ==  |     |  zz  |
X______Y     X______Y
```

- Zauberer: name_
- 0Z: mana_cost_
- X: damage_points_
- Y: current_life_points_
- Optional:
  - `M`  mana_drain_
  - `S`  shield_
  - `==` Creature hat in dieser Runde schon angegriffen
  - `zz` Karte wurde gerade ausgespielt und damit beschworen (Die Kreatur schläft noch bis zur nächsten Runde)





### Spielfeld

```
                                              ______  
H                                            |      | 
A                                            |  05  | 
N                                            |      |     
D                                            |______| 


============================= LP : 20 | MANA : 15/15 =====================================

S                                                                                         

P    ______       ______        ______                                                     
    |      |     |      |      |      |
I   DAVID        SAFRAN        SIMONE
    |      |     |      |      |      |
E   1______2     8______3      1______1                                                   
                                                          
L   ~~~01~~~~~~~~~~~02~~~~~~~~~~~03~~~~~~~~~~~04~~~~~~~~~~~05~~~~~~~~~~~06~~~~~~~~~~~07~~~

F    _____M       ______        ____MS                                                     
    |      |     |      |      |      |
E   DAVID        MICHAEL       SIMONE                                                     
    |      |     |      |      |      |
L   9______2     8______3      1______1                                                   

D                                                                                         

============================ LP : 30 | MANA : 01/15 ======================================

     09___M       03___S        05____        01__MS        09____
H    |      |     |      |      |      |      |      |      |      |
A    DAVID        SAFRAN        ZAUBERER      SIMONE        RELIEF
N    |      |     |      |      |      |      |      |      |      |
D    1______2     8______3      x______x      1______1      x______x


```





