## SpellCard
### HEALER

- Kosten: 5 Mana

`HEALER` versorgt alle Kreaturen auf dem Spielfeld des ziehenden Spielers mit +1 Lebenspunkt. Die maximale Anzahl von 9 Lebenspunkten darf dabei jedoch nicht überschritten werden. Hat eine Kreatur bereits 9 Lebenspunkte, bleiben die Lebenspunkte bei 9.

### RELIEF

- Kosten: 3 Mana

Spielt ein Spieler die Zauberkarte ``RELIEF`` aus, zieht er 3 Karten von seinem Abhebestapel. Sind nicht genügend Karten am Abhebestapel, kostet den ziehenden Spieler jede Karte, die er vergeblich versucht abzuheben, einen Lebenspunkt.

Hat der ziehende Spieler also z.B. nur noch eine Karte am Abhebestapel, erhält er diese Karte auf die Hand und verliert 2 Lebenspunkte.

### REBIRTH

- Kosten: 5 Mana

`REBIRTH` kann verwendet werden, um eine Kreatur wiederzubeleben. Die wiederbelebte Kreatur landet auf der Spielfeldhälfte des ziehenden Spielers auf der freien Position mit der niedrigsten Nummer und hat wieder volles Leben (gleiches Leben als wäre die Karte von der Hand gespielt worden). Die wiederbelebte Kreatur darf in dieser Runde noch nicht angreifen.

`REBIRTH` belebt keine Kreatur wieder, wenn der Friedhof leer ist oder die Spielfeldhälfte des ziehenden Spielers voll mit Kreaturen ist. 

### DRACULA

- Kosten: 2 Mana

`DRACULA`  zieht dem Gegner 2 Lebenspunkte ab und gibt dem ziehenden Spieler 2 Lebenspunkte. Hat also z.B. der ziehende Spieler 24 Lebenspunkte und der Gegner 15, dann hat nach dem Ausspielen von `DRACULA` der ziehende Spieler 26 Lebenspunkte und der Gegner 13. Die Lebenspunkte dürfen auch über die Startlebenspunkte von 30 ansteigen. Hat der Gegner nach dem Ausspielen von `DRACULA` 0 oder -1 Lebenspunkte, so hat der ziehende Spieler gewonnen.



